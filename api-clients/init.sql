CREATE DATABASE IF NOT EXISTS carpart_db;

USE carpart_db;

CREATE USER IF NOT EXISTS 'usertp'@'%' IDENTIFIED BY 'usertp';

GRANT ALL PRIVILEGES ON carpart_db.* TO 'usertp'@'%';

FLUSH PRIVILEGES;
