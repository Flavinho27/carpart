FROM mysql:latest

ENV MYSQL_ROOT_PASSWORD=root
ENV MYSQL_DATABASE=carpart_db
ENV MYSQL_USER=usertp
ENV MYSQL_PASSWORD=usertp

# Copier le script SQL d'initialisation dans le conteneur
COPY ./api-clients/init.sql /docker-entrypoint-initdb.d/

EXPOSE 3306
