import time
from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from dotenv import load_dotenv
import os

# Charger les variables d'environnement communes, .env
load_dotenv()

# Sélectionner le fichier .env en fonction de l'environnement, par défaut .env.dev
environment = os.getenv("APP_ENV", "dev")
dotenv_path = f"./app/.env.{environment}"

# Charger les variables d'environnement spécifiques à l'environnement
load_dotenv(dotenv_path)

# Configuration de la connexion MongoDB
MYSQL_DB_URL = os.getenv("MYSQL_DB_URL")
SQLALCHEMY_DATABASE_URL = MYSQL_DB_URL

engine = create_engine(SQLALCHEMY_DATABASE_URL)
while True:
    try:
        connection = engine.connect()
        connection.close()
        break
    except:
        print("MySQL is not ready yet, waiting 2 seconds...")
        time.sleep(2)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()

class Client(Base):
    __tablename__ = "clients"

    # L'index permet d'accélérer les recherches
    id = Column(Integer, primary_key=True, index=True)
    first_name = Column(String(50), index=True)
    last_name = Column(String(50), index=True)
    email = Column(String(50), unique=True, index=True) # L'adresse email doit être unique
    order_count = Column(Integer)

Base.metadata.create_all(bind=engine, checkfirst=True)
