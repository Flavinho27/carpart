from fastapi import FastAPI, HTTPException, Depends, status
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
from app.client import Client, SessionLocal

app = FastAPI()

######################################################
# Routes de l'API pour la gestion des clients (MySQL)#
######################################################

# Dépendance pour obtenir la session SQLAlchemy (MySQL)
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

class ClientCreate(BaseModel):
    first_name: str
    last_name: str
    email: str
    order_count: int

    def to_client_model(self):
        return Client(**self.dict())

@app.post("/add-client/")
async def add_client(client: ClientCreate, db: Session = Depends(get_db)):
    try:
        # Convertir l'instance de ClientCreate en Client
        client_model = client.to_client_model()
        # Ajouter le client à la base de données
        db.add(client_model)
        db.commit()
        db.refresh(client_model)
        return {"message": "Client added successfully"}
    except IntegrityError as e:
        # Gérer les erreurs liées à l'intégrité des contraintes MySQL
        db.rollback()
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Error: Integrity constraint violation. This email may already exist in the database.",
        )

@app.put("/update-client/{client_id}")
async def update_client(client_id: int, new_data: ClientCreate, db: Session = Depends(get_db)):
    # Rechercher le client existant dans la base de données
    existing_client = db.query(Client).filter(Client.id == client_id).first()

    # Si le client n'est pas trouvé, renvoyer une erreur 404
    if existing_client is None:
        raise HTTPException(status_code=404, detail="Client not found")

    try:
        # Mettre à jour les champs du client avec les nouvelles données
        for field, value in new_data.dict().items():
            setattr(existing_client, field, value)

        # Committer les changements dans la base de données
        db.commit()
        db.refresh(existing_client)
        
        return {"message": "Client updated successfully"}
    except IntegrityError as e:
        # Gérer les erreurs liées à l'intégrité des contraintes MySQL
        db.rollback()
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Error: Integrity constraint violation. This update may violate a unique constraint in the database.",
        )

@app.delete("/delete-client/{client_id}")
async def delete_client(client_id: int, db: Session = Depends(get_db)):
    # Rechercher le client existant dans la base de données
    client = db.query(Client).filter(Client.id == client_id).first()

    # Si le client n'est pas trouvé, renvoyer une erreur 404
    if client is None:
        raise HTTPException(status_code=404, detail="Client not found")

    try:
        # Supprimer le client de la base de données
        db.delete(client)
        db.commit()
        return {"message": "Client deleted successfully"}
    except IntegrityError as e:
        # Gérer les erreurs liées à l'intégrité des contraintes MySQL
        db.rollback()
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Error: Integrity constraint violation. This deletion may violate a constraint in the database.",
        )

@app.get("/get-client/{client_id}")
async def get_client(client_id: int, db: Session = Depends(get_db)):
    # Rechercher le client existant dans la base de données
    client = db.query(Client).filter(Client.id == client_id).first()

    # Si le client n'est pas trouvé, renvoyer une erreur 404
    if client is None:
        raise HTTPException(status_code=404, detail="Client not found")

    # Convertir l'objet client en un format JSON compatible
    return jsonable_encoder(client)
