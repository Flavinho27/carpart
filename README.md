# Projet CarPart API

Ce projet implémente une API web de gestion des stocks de pièces (api_products) et une API de gestion des clients (api_clients) pour l'entreprise CarPart. Les données sont stockées dans des bases de données MongoDB et MySQL respectivement.

## Emplacement du Projet

Le code source de ce projet est hébergé sur GitLab. Vous pouvez accéder au projet en suivant ce lien : [Projet CarPart API sur GitLab](https://gitlab.com/Flavinho27/carpart).

## Conteneurs Docker

Les conteneurs Docker pour ce projet sont disponibles sur Docker Hub et le Container Registry de GitLab. Vous pouvez les récupérer et les utiliser en suivant ces liens :

- [Conteneur api-products sur Docker Hub](https://hub.docker.com/r/flavinho27/tp-virtualisation-api-products/tags)
- [Conteneur api-clients sur Docker Hub](https://hub.docker.com/r/flavinho27/tp-virtualisation-api-clients/tags)
- [Conteneurs sur Container Registry de GitLab](https://gitlab.com/Flavinho27/carpart/container_registry/)

## Conteneurs

Le projet utilise Docker et Docker Compose pour faciliter le déploiement et la gestion des conteneurs. Assurez-vous d'avoir Docker et Docker Compose installés sur votre machine avant de commencer.

## Configuration

### Variables d'environnement

Les variables d'environnements nécessaires sont stockées dans les fichiers .env

## Installation et exécution

1. Construisez et lancez les conteneurs avec Docker Compose :

```bash
docker-compose up --build
```

2. Les API seront disponibles aux adresses suivantes :

   - API Products: [http://localhost:8000](http://localhost:8000)
   - API Clients: [http://localhost:8001](http://localhost:8001)

## Arrêt des conteneurs

Pour arrêter les conteneurs, utilisez la commande suivante :

```bash
docker-compose down
```
