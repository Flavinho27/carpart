FROM python:3.9

WORKDIR /code

COPY ./api-products/requirements.txt /code/requirements.txt

# Installation des dépendances de l'application FastAPI
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

COPY ./api-products/app /code/app

# port sur lequel l'application FastAPI écoute
EXPOSE 8000

ENV APP_ENV=prod

# Commande pour exécuter l'application FastAPI
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8000"]
