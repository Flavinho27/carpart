from fastapi import FastAPI, HTTPException, Depends, Path, status
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
from motor.motor_asyncio import AsyncIOMotorClient
from dotenv import load_dotenv
import os

# Charger les variables d'environnement communes, .env
load_dotenv()

# Sélectionner le fichier .env en fonction de l'environnement, par défaut .env.dev
environment = os.getenv("APP_ENV", "dev")
dotenv_path = f"./app/.env.{environment}"

# Charger les variables d'environnement spécifiques à l'environnement
load_dotenv(dotenv_path)

# Configuration de la connexion MongoDB
MONGO_DB_URL = os.getenv("MONGO_DB_URL")
DATABASE_NAME = os.getenv("DATABASE_NAME")


app = FastAPI()


# Modèle Pydantic pour la création et la mise à jour des produits
class Product(BaseModel):
    name: str
    description: str
    quantity: int

# Dépendance pour obtenir la connexion à la base de données MongoDB
async def get_database():
    client = AsyncIOMotorClient(MONGO_DB_URL)
    database = client[DATABASE_NAME]
    return database

# Fonction d'initialisation de la base de données
async def initialize_database():
    database = await get_database()
    
    # Liste des collections existantes dans la base de données
    existing_collections = await database.list_collection_names()
    
    # Vérifier si la collection "products" existe
    if "products" not in existing_collections:
        # Créer la collection "products" si elle n'existe pas
        await database.create_collection("products")

# Utilisez l'event "startup" de FastAPI pour appeler la fonction d'initialisation
@app.on_event("startup")
async def startup_event():
    await initialize_database()

#########################################################
# Routes de l'API pour la gestion des produits (MongoDB)#
#########################################################

@app.post("/add-product/")
async def add_product(product: Product, db: AsyncIOMotorClient = Depends(get_database)):
    try:
        collection = db["products"]
        
        if await collection.count_documents({"name": product.name}) > 0:
            raise HTTPException(status_code=400, detail="Product already exists")

        await collection.insert_one(jsonable_encoder(product))
        return {"message": "Product added successfully"}
    except Exception as e:
        # Capturer toute exception et renvoyer une erreur 500 (Internal Server Error)
        raise HTTPException(status_code=500, detail=f"Internal Server Error: {str(e)}")

@app.put("/update-product/{product_name}")
async def update_product(product_name: str, product: Product, db: AsyncIOMotorClient = Depends(get_database)):
    try:
        collection = db["products"]
        
        existing_product = await collection.find_one({"name": product_name})
        
        if existing_product is None:
            raise HTTPException(status_code=404, detail="Product not found")
        
        # Mise à jour des champs spécifiés
        updated_product = existing_product.copy()
        updated_product.update(jsonable_encoder(product))

        await collection.replace_one({"name": product_name}, updated_product)
        
        return {"message": "Product updated successfully"}
    except Exception as e:
        # Capturer toute exception et renvoyer une erreur 500 (Internal Server Error)
        raise HTTPException(status_code=500, detail=f"Internal Server Error: {str(e)}")

@app.delete("/delete-product/{product_name}")
async def delete_product(product_name: str, db: AsyncIOMotorClient = Depends(get_database)):
    try:
        collection = db["products"]
        
        result = await collection.delete_one({"name": product_name})
        print(result.deleted_count  )
        
        if result.deleted_count == 0:
            print("Product not found")
            raise HTTPException(status_code=404, detail="Product not found")
        
        return {"message": "Product deleted successfully"}
    except Exception as e:
        if "Product not found" in str(e.detail):
            raise HTTPException(status_code=404, detail="Product not found")
        else:
            # Capturer toute exception et renvoyer une erreur 500 (Internal Server Error)
            raise HTTPException(status_code=500, detail=f"Internal Server Error: {str(e.detail)}")


@app.get("/get-product/{product_name}")
async def get_product(product_name: str = Path(..., title="The name of the product to get"), db: AsyncIOMotorClient = Depends(get_database)):
    collection = db["products"]
    
    # Utilisation de la méthode find_one pour obtenir un seul document
    product = await collection.find_one({"name": product_name}, projection={"_id": 0})

    if product is None:
        # Utilisation de HTTPException pour renvoyer une erreur 404
        raise HTTPException(status_code=404, detail=f"Product '{product_name}' not found")
    
    return product

@app.get("/get-products")
async def get_products(db: AsyncIOMotorClient = Depends(get_database)):
    try:
        collection = db["products"]
        
        # Utilisation d'une projection pour exclure le champ "_id"
        projection = {"_id": 0}
        
        # Utilisation de to_list avec un filtre pour exclure les éléments None
        products = await collection.find(projection=projection).to_list(length=None)
        products = [product for product in products if product is not None]
        
        products = jsonable_encoder(products, by_alias=True)
        
        return products
    except Exception as e:
        # Capturer toute exception et renvoyer une erreur 500 (Internal Server Error)
        raise HTTPException(status_code=500, detail=f"Internal Server Error: {str(e)}")
